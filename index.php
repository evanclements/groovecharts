<?php

set_include_path("/home/evan/Desktop/Projects/groovecharts/");

include("Requests/requests.php");
include("config.php");
Requests::register_autoloader();

// Grooveshark Lib

$lastFm_username = $_REQUEST['who'];
$timeframe = $_REQUEST['when'];

$base = "http://ws.audioscrobbler.com/2.0/user/".$lastFm_username."/";

switch($timeframe){
	case "wk":
		$url = $base."weeklytrackchart.xml";
	break;
	
	case "quarter":
		$url = $base."toptracks.xml?period=3month";
	break;
	
	case "half":
		$url = $base."toptracks.xml?period=6month";
	break;
	
	case "whole":
		$url = $base."toptracks.xml?period=12month";
	break;
	
	case "overall":
		$url = $base."toptracks.xml";
	break;
	
	default:
		$url = $base."toptracks.xml";
}



$chart = simplexml_load_file($url);



$list = array();
for ($i=0; $i<count($chart->track); $i++) { 
	if($timeframe !== "wk"){
		$artist = (string)$chart->track[$i]->artist->name;
	}else{
		$artist = (string)$chart->track[$i]->artist;
	}
	$track = (string)$chart->track[$i]->name;
	
	$list[$i] = "{$artist} {$track}";
}

/**
 * Grooveshark Flow
 * 
 * 1. Start a Grooveshark Session
 * 2. Save SessionID into variable 'gsSessId'
 * 3. Search up songs in Last.fm chart(s)
 * 4. Build array of songs to add to playlist
 * 5. Use array to build a playlist
 * 6. Provide link to playlist
 * 7. ???
 * 8. Profit!
 * 
 */
$gsSessId_data = Requests::post($credentials['gsUrl'], array(), array("method" => "addUserFavoriteSong", "header" => array("wsKey" => $credentials['gsKey'])));
$gsSessId = $gsSessId_data->body;

error_log($gsSessId);

// POST payload
/*array(
	"method" => "addUserFavoriteSong",
	"parameters" => array(
		"songID" => 30547543
		), 
	"header" => array(
		"wsKey" => $credentials['gsKey'],
		"sessionID" => 'df8fec35811a6b240808563d9f72fa2'
		)
	);*/
